FROM centos:7

RUN yum install -y git && \
  yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo && \
  yum install -y docker-ce-cli && \
  yum clean all

COPY scripts/publish_latest /usr/local/bin/